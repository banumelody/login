<!DOCTYPE html>
<html>
<head>
	<title>kuliah web</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>
	<div id="main">
		
		<div id="nav-bar">
			<div id="logo">
				<h1>My Website</h1>
			</div>
			<div id="nav-menu">
				<ul>
					<li><a href="#home">Home</a></li>
					<li><a href="#news">News</a></li>
					<li><a href="#contact">Contact</a></li>
					<li><a href="#about">About</a></li>
				</ul>
			</div>
		</div>

		<div id="container">
			<div id="left-menu">
				<ul>
					<li><a href="#home">Home</a></li>
					<li><a href="#news">News</a></li>
					<li><a href="#contact">Contact</a></li>
					<li><a href="#about">About</a></li>
				</ul>
			</div>
			<div id="main-content">
				<div class="inner-content">
					<h2>Login</h2>
					<hr />
					<form action="post.php" method="post">
						<div>
							<labelfor="inputEmail">Email</label>
							<div>
								<input type="text" id="inputEmail" placeholder="Email" name="email">
							</div>
						</div>
						<div>
							<labelfor="inputPassword">Password</label>
							<div>
								<input type="password" id="inputPassword" placeholder="Password" name="password">
							</div>
						</div>
						<div>
							<div >
								<label>
									<input type="checkbox"> Remember me
								</label>
								<button type="submit">Sign in</button>
							</div>
						</div>
					</form>
				</div> <!-- end of inner content -->
			</div>
		</div>

		<div id="foot-bar">
			footer
		</div>

	</div>
</body>
</html>